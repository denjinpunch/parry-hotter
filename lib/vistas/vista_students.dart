
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:harry_potter/caracteristicas/characters.dart';
import 'package:harry_potter/caracteristicas/bloc.dart';

class VistaStudents extends StatelessWidget {
  late List<Character> list;
  VistaStudents(this.list, {super.key});
  @override
  Widget build(BuildContext context) {
    final elBloc = context.read<ClaseBloc>();
    return MaterialApp(
      title: "Estudiantes",
      home: Scaffold(
        appBar: AppBar(
          title: Text("Estudiantes"),
           leading: IconButton(
                    icon: const Icon(Icons.arrow_back_ios),
                    iconSize: 20.0,
                    onPressed: () {
                      elBloc.add(Creado());
                    },
                  ),
        ),
        body: ListView.builder(
          itemCount: list.length,
          itemBuilder: (context, index) {
            return ListTile(
              leading: Image(image: NetworkImage(list[index].image)),
              title: Text(list[index].name),
              subtitle: Text(
                  " Gender: ${list[index].gender}\n Species: ${list[index].species}\n House: ${list[index].house}\n Date of Birth: ${list[index].dateOfBirth}\n Wizard? ${list[index].wizard}\n Ancestry: ${list[index].ancestry}\n Eye Color: ${list[index].eyeColour}\n Hair Color: ${list[index].hairColour}\n Wand Desc.:${list[index].wand}\n Patronus: ${list[index].patronus}\n Student? ${list[index].hogwartsStudent}\n Staff? ${list[index].hogwartsStaff}\n Actor: ${list[index].actor}\n Alive? ${list[index].alive}"),
            );
          },
        ),
      ),
    );
  }
}