import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../caracteristicas/bloc.dart';

class VistaPrincipal extends StatelessWidget {
  const VistaPrincipal({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 200,
            height: 35,
            child: ElevatedButton(
                onPressed: () {
                  var bloc = context.read<ClaseBloc>();
                  bloc.add(CargarPersonajes());
                },
                child: const Text('Ver Personajes')),
          ),
          SizedBox(height: 15),
          Container(
            width: 200,
            height: 35,
            child: ElevatedButton(
                onPressed: () {
                  var bloc = context.read<ClaseBloc>();
                  bloc.add(CargarSpells());
                },
                child: Text('Ver Hechizos')),
          ),
          SizedBox(height: 15),
          Container(
            width: 200,
            height: 35,
            child: ElevatedButton(
                onPressed: () {
                  var bloc = context.read<ClaseBloc>();
                  bloc.add(CargarBuscador());
                },
                child: Text('Buscador de Casas')),
          ),
          SizedBox(height: 15),
          Container(
            width: 200,
            height: 35,
            child: ElevatedButton(
                onPressed: () {
                  var bloc = context.read<ClaseBloc>();
                  bloc.add(CargaStudents());
                },
                child: Text('Ver Estudiantes')),
          ),
          SizedBox(height: 15),
          Container(
            width: 200,
            height: 35,
            child: ElevatedButton(
                onPressed: () {
                  var bloc = context.read<ClaseBloc>();
                  bloc.add(CargarStaff());
                },
                child: Text('Ver Personal')),
          ),
        ],
      )),
    );
  }
}
