
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:harry_potter/caracteristicas/spells.dart';
import 'package:harry_potter/caracteristicas/bloc.dart';

class VistaSpells extends StatelessWidget {
  late List<Spells> list;
  VistaSpells(this.list, {super.key});
  @override
  Widget build(BuildContext context) {
    final elBloc = context.read<ClaseBloc>();
    return MaterialApp(
      title: "Hechizos",
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hechizos"),
           leading: IconButton(
                    icon: const Icon(Icons.arrow_back_ios),
                    iconSize: 20.0,
                    onPressed: () {
                      elBloc.add(Creado());
                    },
                  ),
        ),
        body: ListView.builder(
          itemCount: list.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(list[index].namespell),
              subtitle: Text(
                  list[index].description),
            );
          },
        ),
      ),
    );
  }
}