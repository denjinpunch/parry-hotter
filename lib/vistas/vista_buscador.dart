import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:harry_potter/caracteristicas/bloc.dart';
import 'package:harry_potter/dominio/obt_casa.dart';

class VistaBuscador extends StatefulWidget {
  const VistaBuscador({Key? key}) : super(key: key);

  @override
  State<VistaBuscador> createState() => _VistaBuscadorState();
}

class _VistaBuscadorState extends State<VistaBuscador> {
  bool _validation = false;
  final _controlador = TextEditingController();
 @override
  void initState() {
    _controlador.addListener(listener);
    super.initState();
  }
   void listener() {
    setState(() {
      try {
        ObtenerCasa.constructor(_controlador.text);
        _validation = true;
      } catch (e) {
        _validation = false;
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    final elBloc = context.read<ClaseBloc>();
    return MaterialApp(
            home: Scaffold(
        appBar: AppBar(
          title: Text("Buscador"),
           leading: IconButton(
                    icon: const Icon(Icons.arrow_back_ios),
                    iconSize: 20.0,
                    onPressed: () {
                      elBloc.add(Creado());
                    },
                  ),
        ),
      
       body: Column(
       children: [const Text('Dame el nombre de la casa'),
        TextField(
          controller: _controlador,
        ),
        Container(
            child: _validation
                ? null
                : TextButton(
                    style: ButtonStyle(
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                    ),
                    onPressed: null,
                    child: const Text('Ingresar'),
                  )),
        Container(
            child: !_validation
                ? null
                : TextButton(
                    style: ButtonStyle(
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.blue),
                    ),
                    onPressed: () {
                      elBloc.add(NombreRecibido(ObtenerCasa.constructor(_controlador.text)));
                    },
                    child: const Text('Ingresar'),
                  ))
    ])));
  }
  @override
  void dispose() {
    _controlador.dispose();
    super.dispose();
  }
}