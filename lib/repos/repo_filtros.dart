import 'dart:io';
import 'package:fpdart/fpdart.dart';
import 'package:harry_potter/dominio/problems.dart';
import 'dart:convert';
import 'package:harry_potter/caracteristicas/characters.dart';
import 'package:http/http.dart' as http;

abstract class RepositorioStudents {
  Future<Either<Problema, List<Character>>> obtenerStudents();
}

abstract class RepositorioStudentsPrueba {
  Future<Either<Problema, List<Character>>> obtenerStudentsOffline();
}

abstract class RepositorioStaff {
  Future<Either<Problema, List<Character>>> obtenerStaff();
}

abstract class RepositorioStaffPrueba {
  Future<Either<Problema, List<Character>>> obtenerStaffOffline();
}

class OnlineStudents extends RepositorioStudents {
  @override
  Future<Either<Problema, List<Character>>> obtenerStudents() async {
    try {
      Uri direccion =
          Uri.parse('https://hp-api.onrender.com/api/characters/students');
      final respuesta = await http.get(direccion);
      if (respuesta.statusCode != 200) {
        return left(ServidorNoAlcanzado());
      }
      final envio = jsonDecode(respuesta.body);
      List<Character> lista = [];
      Character c;
      for (int i = 0; i < envio.length; i++) {
        String proimage = envio[i]["image"];
        if (proimage.isEmpty) {
          proimage = envio[i]["image"].toString().replaceAll("",
              "https://images.amcnetworks.com/amc.com/wp-content/uploads/2015/04/cast_bb_700x1000_walter-white-lg.jpg");
        }
        c = Character.constructor(
            proname: envio[i]["name"],
            prospecies: envio[i]["species"],
            progender: envio[i]["gender"],
            prohouse: envio[i]["house"],
            prodateOfBirth: envio[i]["dateOfBirth"],
            prowizard: envio[i]["wizard"],
            proancestry: envio[i]["ancestry"],
            proeyeColour: envio[i]["eyeColour"],
            prohairColour: envio[i]["hairColour"],
            prowand: envio[i]["wand"],
            propatronus: envio[i]["patronus"],
            prohogwartsStudent: envio[i]["hogwartsStudent"],
            prohogwartsStaff: envio[i]["hogwartsStaff"],
            proactor: envio[i]["actor"],
            proalive: envio[i]["alive"],
            proimage: proimage.replaceAll("herokuapp", "onrender"));
        lista.add(c);
      }
      return Right(lista);
    } catch (e) {
      return Left(VersionIncorrecta());
    }
  }
}

class OfflineStudents extends RepositorioStudentsPrueba {
  @override
  Future<Either<Problema, List<Character>>> obtenerStudentsOffline() async {
    try {
      List<dynamic> json =
          jsonDecode((File("./test/json/students.json").readAsStringSync()));
      List<Character> lista = [];
      Character c;
      for (int i = 0; i < json.length; i++) {
        String proimage = json[i]["image"];
        if (proimage.isEmpty) {
          proimage = json[i]["image"].toString().replaceAll("",
              "https://images.amcnetworks.com/amc.com/wp-content/uploads/2015/04/cast_bb_700x1000_walter-white-lg.jpg");
        }
        c = Character.constructor(
            proname: json[i]["name"],
            prospecies: json[i]["species"],
            progender: json[i]["gender"],
            prohouse: json[i]["house"],
            prodateOfBirth: json[i]["dateOfBirth"],
            prowizard: json[i]["wizard"],
            proancestry: json[i]["ancestry"],
            proeyeColour: json[i]["eyeColour"],
            prohairColour: json[i]["hairColour"],
            prowand: json[i]["wand"],
            propatronus: json[i]["patronus"],
            prohogwartsStudent: json[i]["hogwartsStudent"],
            prohogwartsStaff: json[i]["hogwartsStaff"],
            proactor: json[i]["actor"],
            proalive: json[i]["alive"],
            proimage: proimage.replaceAll("herokuapp", "onrender"));
        lista.add(c);
      }
      return Right(lista);
    } catch (e) {
      return Left(VersionIncorrecta());
    }
  }
}

class OnlineStaff extends RepositorioStaff {
  @override
  Future<Either<Problema, List<Character>>> obtenerStaff() async {
    try {
      Uri direccion =
          Uri.parse('https://hp-api.onrender.com/api/characters/staff');
      final respuesta = await http.get(direccion);
      if (respuesta.statusCode != 200) {
        return left(ServidorNoAlcanzado());
      }
      final envio = jsonDecode(respuesta.body);
      List<Character> lista = [];
      Character c;
      for (int i = 0; i < envio.length; i++) {
        String proimage = envio[i]["image"];
        if (proimage.isEmpty) {
          proimage = envio[i]["image"].toString().replaceAll("",
              "https://images.amcnetworks.com/amc.com/wp-content/uploads/2015/04/cast_bb_700x1000_walter-white-lg.jpg");
        }
        c = Character.constructor(
            proname: envio[i]["name"],
            prospecies: envio[i]["species"],
            progender: envio[i]["gender"],
            prohouse: envio[i]["house"],
            prodateOfBirth: envio[i]["dateOfBirth"],
            prowizard: envio[i]["wizard"],
            proancestry: envio[i]["ancestry"],
            proeyeColour: envio[i]["eyeColour"],
            prohairColour: envio[i]["hairColour"],
            prowand: envio[i]["wand"],
            propatronus: envio[i]["patronus"],
            prohogwartsStudent: envio[i]["hogwartsStudent"],
            prohogwartsStaff: envio[i]["hogwartsStaff"],
            proactor: envio[i]["actor"],
            proalive: envio[i]["alive"],
            proimage: proimage.replaceAll("herokuapp", "onrender"));
        lista.add(c);
      }
      return Right(lista);
    } catch (e) {
      return Left(VersionIncorrecta());
    }
  }
}

class OfflineStaff extends RepositorioStaffPrueba {
  @override
  Future<Either<Problema, List<Character>>> obtenerStaffOffline() async {
    try {
      List<dynamic> json =
          jsonDecode((File("./test/json/staff.json").readAsStringSync()));
      List<Character> lista = [];
      Character c;
      for (int i = 0; i < json.length; i++) {
        String proimage = json[i]["image"];
        if (proimage.isEmpty) {
          proimage = json[i]["image"].toString().replaceAll("",
              "https://images.amcnetworks.com/amc.com/wp-content/uploads/2015/04/cast_bb_700x1000_walter-white-lg.jpg");
        }
        c = Character.constructor(
            proname: json[i]["name"],
            prospecies: json[i]["species"],
            progender: json[i]["gender"],
            prohouse: json[i]["house"],
            prodateOfBirth: json[i]["dateOfBirth"],
            prowizard: json[i]["wizard"],
            proancestry: json[i]["ancestry"],
            proeyeColour: json[i]["eyeColour"],
            prohairColour: json[i]["hairColour"],
            prowand: json[i]["wand"],
            propatronus: json[i]["patronus"],
            prohogwartsStudent: json[i]["hogwartsStudent"],
            prohogwartsStaff: json[i]["hogwartsStaff"],
            proactor: json[i]["actor"],
            proalive: json[i]["alive"],
            proimage: proimage.replaceAll("herokuapp", "onrender"));
        lista.add(c);
      }
      return Right(lista);
    } catch (e) {
      return Left(VersionIncorrecta());
    }
  }
}
