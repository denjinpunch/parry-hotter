import 'dart:io';
import 'package:fpdart/fpdart.dart';
import 'package:harry_potter/dominio/problems.dart';
import 'dart:convert';
import 'package:harry_potter/caracteristicas/spells.dart';
import 'package:http/http.dart' as http;
abstract class RepositorioSpell {
  Future<Either<Problema, List<Spells>>> obtenerSpells();
}
abstract class RepositorioSpellPrueba {
  Future<Either<Problema, List<Spells>>> obtenerSpellsPrueba();
}
class OnlineSpell extends RepositorioSpell{
  @override
 Future<Either<Problema, List<Spells>>> obtenerSpells() async {
     try {
    Uri direccion = Uri.parse('https://hp-api.onrender.com/api/spells');
    final respuesta = await http.get(direccion);
    if (respuesta.statusCode != 200) {
      return left(ServidorNoAlcanzado());
    }
    final envio = jsonDecode(respuesta.body);
    List<Spells> lista =[];
      Spells s;
      for (int i=0; i< envio.length; i++) {
        s = Spells.constructor(
        pronamespell: envio[i]["name"], 
        prodescription: envio[i]["description"] 
        );
        lista.add(s);
        print(lista[i].namespell);
        print(lista[i].description);
      } 
      return Right(lista);
    } catch (e) {
      return Left(VersionIncorrecta());
    }
  }
}
class OfflineSpells extends RepositorioSpellPrueba{
  @override
 Future<Either<Problema, List<Spells>>> obtenerSpellsPrueba() async {
    try {  
      List<dynamic> json= jsonDecode((File("./test/json/spells.json").readAsStringSync()));
      List<Spells> lista =[];
      Spells s;
      for (int i=0; i<json.length; i++) {
        s = Spells.constructor(
        pronamespell: json[i]["name"], 
        prodescription: json[i]["description"], 
        );
        lista.add(s);
        print(lista[i].namespell);
        print(lista[i].description);
      }
     return  Right(lista);
    } catch (e) {
      return Left(VersionIncorrecta());
    }
  }
}