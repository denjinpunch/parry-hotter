import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:harry_potter/caracteristicas/bloc.dart';
import 'package:harry_potter/vistas/vista_buscador.dart';
import 'package:harry_potter/vistas/vista_characters.dart';
import 'package:harry_potter/vistas/vista_creandose.dart';
import 'package:harry_potter/vistas/vista_escuela.dart';
import 'package:harry_potter/vistas/vista_principal.dart';
import 'package:harry_potter/vistas/vista_spells.dart';
import 'package:harry_potter/vistas/vista_staff.dart';
import 'package:harry_potter/vistas/vista_students.dart';
//flutter run -d chrome --web-renderer html
void main() {
  runApp(const AplicacionInyectada());
}

class AplicacionInyectada extends StatelessWidget {
  const AplicacionInyectada({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        ClaseBloc blocVerificacion = ClaseBloc();
        Future.delayed(const Duration(seconds: 2), () {
          blocVerificacion.add(Creado());
        });
        return blocVerificacion;
      },
      child: const Aplicacion(),
    );
  }
}

class Aplicacion extends StatelessWidget {
  const Aplicacion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Builder(builder: (context) {
          var estado = context.watch<ClaseBloc>().state;
          if (estado is Creandose) {
            return const VistaCreandose();
          }
          if (estado is VistaP) {
            return const VistaPrincipal();
          }
          if (estado is MostrarPersonajes) {
            return VistaPersonajes(estado.listCharacters);
          }
          if (estado is MostrarSpells) {
            return VistaSpells(estado.listSpells);
          }
          if (estado is MostrarBuscador) {
            return const VistaBuscador();
          }
          if (estado is MostrarCasa) {
            return  VistaEscuela(estado.listHouse);
          }
          if (estado is MostrarStudents) {
            return  VistaStudents(estado.listStudents);
          }
          if (estado is MostrarStaff) {
            return VistaStaff(estado.listStaff);
          }
          return const Center(child: Text('Espera... si ya tardo valiste LOL'));
        }),
      ),
    );
  }
}
