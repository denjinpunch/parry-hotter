abstract class Problema {}

class NoExiste extends Problema {}

class VersionIncorrecta extends Problema {}

class ServidorNoAlcanzado extends Problema {}