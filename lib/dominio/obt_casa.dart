class ObtenerCasa {
  late final String valor;
  ObtenerCasa._(this.valor);
  factory ObtenerCasa.constructor(String casa) {
    if (casa.trim().isEmpty) {
      throw ('No encontrado');
    }
    return ObtenerCasa._(casa);
  }
}