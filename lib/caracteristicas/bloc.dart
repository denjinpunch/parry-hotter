import 'package:bloc/bloc.dart';
import 'package:fpdart/fpdart.dart';
import 'package:harry_potter/caracteristicas/characters.dart';
import 'package:harry_potter/caracteristicas/spells.dart';
import 'package:harry_potter/dominio/obt_casa.dart';
import 'package:harry_potter/dominio/problems.dart';
import 'package:harry_potter/repos/repo_casa.dart';
import 'package:harry_potter/repos/repo_characters.dart';
import 'package:harry_potter/repos/repo_filtros.dart';
import 'package:harry_potter/repos/repo_spells.dart';

class EventoVerificacion {}

class Creado extends EventoVerificacion {}

class NombreRecibido extends EventoVerificacion {
  final ObtenerCasa casa;
  NombreRecibido(this.casa);
}

class CargarPersonajes extends EventoVerificacion {}

class CargarSpells extends EventoVerificacion {}

class CargarBuscador extends EventoVerificacion {}

class CargaStudents extends EventoVerificacion {}

class CargarStaff extends EventoVerificacion {}

class EstadoVerificacion {}

class Creandose extends EstadoVerificacion {}

class VistaP extends EstadoVerificacion {}

class EsperandoConfirmacionNombre extends EstadoVerificacion {}

class MostrarPersonajes extends EstadoVerificacion {
  final List<Character> listCharacters;
  MostrarPersonajes({required this.listCharacters});
}

class MostrarSpells extends EstadoVerificacion {
  final List<Spells> listSpells;
  MostrarSpells({required this.listSpells});
}

class MostrarBuscador extends EstadoVerificacion {}

class MostrarCasa extends EstadoVerificacion {
  final List<Character> listHouse;
  MostrarCasa({required this.listHouse});
}

class MostrarStudents extends EstadoVerificacion {
  final List<Character> listStudents;
  MostrarStudents({required this.listStudents});
}

class MostrarStaff extends EstadoVerificacion {
  final List<Character> listStaff;
  MostrarStaff({required this.listStaff});
}

class MostrarError extends EstadoVerificacion {}

class ClaseBloc extends Bloc<EventoVerificacion, EstadoVerificacion> {
  OnlineCharacter character = OnlineCharacter();
  OnlineSpell spell = OnlineSpell();
  OnlineCasa house = OnlineCasa();
  OnlineStudents students = OnlineStudents();
  OnlineStaff staff = OnlineStaff();
  ClaseBloc() : super(Creandose()) {
    on<Creado>((event, emit) {
      emit(VistaP());
    });
    on<CargarPersonajes>(((event, emit) async {
      var resultado = await character.obtenerCharacter();
      resultado.match((l) {
        assert(false);
      }, (r) {
        emit(MostrarPersonajes(listCharacters: r));
      });
    }));
    on<CargarSpells>(((event, emit) async {
      var resultado = await spell.obtenerSpells();
      resultado.match((l) {
        assert(false);
      }, (r) {
        emit(MostrarSpells(listSpells: r));
      });
    }));
    on<CargarBuscador>(((event, emit) async {
        emit(MostrarBuscador());
    }));
    on<NombreRecibido>(((event, emit) async {
      emit(EsperandoConfirmacionNombre());
      final resultado =
          await house.obtenerCasa(event.casa);
      resultado.match((l) {
        if (l is NoExiste) {
          emit(Creandose());
        }
      }, (r) {
        emit(MostrarCasa(listHouse: r));
      });
    }));
     on<CargaStudents>(((event, emit) async {
      var resultado = await students.obtenerStudents();
      resultado.match((l) {
        assert(false);
      }, (r) {
        emit(MostrarStudents(listStudents: r));
      });
    }));
     on<CargarStaff>(((event, emit) async {
      var resultado = await staff.obtenerStaff();
      resultado.match((l) {
        assert(false);
      }, (r) {
        emit(MostrarStaff(listStaff: r));
      });
    }));
  }
}
