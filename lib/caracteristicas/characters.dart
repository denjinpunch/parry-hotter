class Character {
  late final String name;
  late final String species;
  late final String gender;
  late final String house;
  late final String dateOfBirth;
  late final bool wizard;
  late final String ancestry;
  late final String eyeColour;
  late final String hairColour;
  late final Map wand;
  late final String patronus;
  late final bool hogwartsStudent;
  late final bool hogwartsStaff;
  late final String actor;
  late final bool alive;
  late final String image;
Character._({
    required this.name,
    required this.species,
    required this.gender,
    required this.house,
    required this.dateOfBirth,
    required this.wizard,
    required this.ancestry,
    required this.eyeColour,
    required this.hairColour,
    required this.wand,
    required this.patronus,
    required this.hogwartsStudent,
    required this.hogwartsStaff,
    required this.actor,
    required this.alive,
    required this.image});
factory Character.constructor({
    required String proname,
    required String prospecies,
    required String progender,
    required String prohouse,
    required String prodateOfBirth,
    required bool prowizard,
    required String proancestry,
    required String proeyeColour,
    required String prohairColour,
    required Map prowand,
    required String propatronus,
    required bool prohogwartsStudent,
    required bool prohogwartsStaff,
    required String proactor,
    required bool proalive,
    required String proimage}) {

return Character._(
   name: proname,
  species: prospecies,
    gender: progender,
    house: prohouse,
    dateOfBirth: prodateOfBirth,
    wizard: prowizard,
    ancestry: proancestry,
    eyeColour: proeyeColour,
    hairColour: prohairColour,
    wand : prowand,
    patronus: propatronus,
    hogwartsStudent: prohogwartsStudent,
    hogwartsStaff: prohogwartsStaff,
    actor: proactor,
    alive: proalive,
    image: proimage
);
}
}