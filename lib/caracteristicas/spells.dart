class Spells {
  late final String namespell;
  late final String description;
Spells._({
    required this.namespell,
    required this.description});
factory Spells.constructor({
    required String pronamespell,
    required String prodescription,}) {
return Spells._(
   namespell: pronamespell,
   description: prodescription
);
}
}