import 'package:harry_potter/dominio/obt_casa.dart';
import 'package:harry_potter/repos/repo_casa.dart';
import 'package:test/test.dart';
import 'package:harry_potter/caracteristicas/characters.dart';
void main() {
  test('Probar gryffindor online', () async {
    OnlineCasa repositorio = OnlineCasa();
    final resultado =
        await repositorio.obtenerCasa(ObtenerCasa.constructor("gryffindor"));
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) => expect(r.length, equals(19014)));
  });

  test('Probar slytherin online', () async {
    OnlineCasa repositorio = OnlineCasa();
    final resultado =
        await repositorio.obtenerCasa(ObtenerCasa.constructor("slytherin"));
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) => expect(r.length, equals(17819)));
  });
  test('probar las instancias de house offline', () async {
  OfflineCasa p = OfflineCasa();
var resultado = await p.obtenerCasaPrueba(ObtenerCasa.constructor("slytherin"));
  resultado.match((l){
assert(false);
  }, (r){   
expect(r, isA<List<Character>>());
  });
});

test('probar las instancias de house online', () async {
  OnlineCasa p = OnlineCasa();
var resultado = await p.obtenerCasa(ObtenerCasa.constructor("slytherin"));
  resultado.match((l){
assert(false);
  }, (r){   
expect(r, isA<List<Character>>());
  });
});
}
