import 'dart:math';
import 'package:test/test.dart';
import 'package:harry_potter/caracteristicas/characters.dart';
import 'package:harry_potter/repos/repo_filtros.dart';

void main() {
test('probar las instancias de Students offline', () async {
  OfflineStudents p = OfflineStudents();
var resultado = await p.obtenerStudentsOffline();
  resultado.match((l){
assert(false);
  }, (r){   
expect(r, isA<List<Character>>());
print(r[0].name);
  });
});
test('probar las instancias de Students online', () async {
  OnlineStudents p = OnlineStudents();
var resultado = await p.obtenerStudents();
  resultado.match((l){
assert(false);
  }, (r){   
expect(r, isA<List<Character>>());
print(r[0].name);
  });
});
test('probar las instancias de Staff offline', () async {
  OfflineStaff p = OfflineStaff();
var resultado = await p.obtenerStaffOffline();
  resultado.match((l){
assert(false);
  }, (r){   
expect(r, isA<List<Character>>());
print(r[0].name);
  });
});
test('probar las instancias de Staff online', () async {
  OnlineStaff p = OnlineStaff();
var resultado = await p.obtenerStaff();
  resultado.match((l){
assert(false);
  }, (r){   
expect(r, isA<List<Character>>());
print(r[0].name);
  });
});
}