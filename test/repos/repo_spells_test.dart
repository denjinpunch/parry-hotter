import 'dart:math';
import 'package:flutter_test/flutter_test.dart';
import 'package:harry_potter/caracteristicas/spells.dart';
import 'package:harry_potter/repos/repo_spells.dart';

void main() {
test('probar las instancias de spells offline', () async {
  OfflineSpells p = OfflineSpells();
var resultado = await p.obtenerSpellsPrueba();
  resultado.match((l){
assert(false);
  }, (r){   
expect(r, isA<List<Spells>>());
  });
});
test('probar las instancias de spells online', () async {
  OnlineSpell p = OnlineSpell();
var resultado = await p.obtenerSpells();
  resultado.match((l){
assert(false);
  }, (r){   
expect(r, isA<List<Spells>>());
  });
});
}