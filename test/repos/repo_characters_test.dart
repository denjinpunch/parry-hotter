import 'dart:math';
import 'package:test/test.dart';
import 'package:harry_potter/caracteristicas/characters.dart';
import 'package:harry_potter/repos/repo_characters.dart';

void main() {
test('probar las instancias de characters offline', () async {
  OfflineCharacter p = OfflineCharacter();
var resultado = await p.obtenerCharacterOffline();
  resultado.match((l){
assert(false);
  }, (r){   
expect(r, isA<List<Character>>());
print(r[0].image);
  });
});
test('probar las instancias de characters online', () async {
  OnlineCharacter p = OnlineCharacter();
var resultado = await p.obtenerCharacter();
  resultado.match((l){
assert(false);
  }, (r){   
expect(r, isA<List<Character>>());
print(r[0].image);
  });
});
}